package com.breakday.etag;

import com.breakday.etag.h2.TagService;
import com.halation.sdk.common.PackageCacheMap;
import com.halation.sdk.entity.TagEntity;
import com.halation.smartmk.business.entity.RefreshBusiness;
import com.halation.smartmk.business.entity.RefreshQueueData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CacheDataThread extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(CacheDataThread.class);

    private TagService tagService;

    private MyProperties myProperties;

    public void init(TagService tagService, MyProperties myProperties) {
        this.tagService = tagService;
        this.myProperties = myProperties;
    }

    public void run() {
        while (true) {
            RefreshQueueData outPackage = PackageCacheMap.getOutPackage();

            if (outPackage != null) {
                List<RefreshBusiness> stationDataList = outPackage.getRefreshBusinessList();

                for (RefreshBusiness refreshBusiness : stationDataList) {
                    int successCount = 0;
                    int failCount = 0;
                    List<TagEntity> templateDataList = refreshBusiness.getTemplateDataList();
                    for (TagEntity tagEntity : templateDataList) {
                        logger.info("【ET】tagEntity's station " +
                                tagEntity.getApNo() + " tagId : " +
                                tagEntity.getTagId() + " TagStatus : " +
                                tagEntity.getStatus());
                        if ("Success".equals(tagEntity.getStatus())) {
                            successCount++;
                            updateSuccessTag(tagEntity.getTagId(), tagEntity.getApNo());
                        } else {
                            failCount++;
                            updateFailTag(tagEntity.getTagId(), tagEntity.getApNo(), tagEntity.getStatus());
                        }
                    }
                    logger.info("【ET】station " + refreshBusiness.getApNo() + " All Count : " + refreshBusiness.getTemplateDataList().size());
                    logger.info("【ET】station " + refreshBusiness.getApNo() + " Success Count : " + successCount);
                    logger.info("【ET】station " + refreshBusiness.getApNo() + " Fail Count : " + failCount);
                }

            }

            try {
                Thread.sleep(myProperties.resultProcessSleep);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void updateFailTag(String tag, String station, String failure) {
        Date date = Calendar.getInstance().getTime();
        tagService.updateTag(tag, station, 1, 1, failure, date, 0);//sendstatus:0：需要发送；1：发送失败；2：发送成功
    }

    private void updateSuccessTag(String tag, String station) {
        Date date = Calendar.getInstance().getTime();
        tagService.updateTag(tag, station, 0, 2, "OK", date, 1);//sendstatus:0：需要发送；1：发送失败；2：发送成功
    }

}