package com.breakday.etag;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ETApplication {

    public static void main(String[] args) {
        SpringApplication.run(ETApplication.class, args);
    }

}