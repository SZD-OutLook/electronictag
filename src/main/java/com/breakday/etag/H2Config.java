package com.breakday.etag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.Map;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactoryH2",
        transactionManagerRef = "transactionManagerH2",
        basePackages = {"com.breakday.etag.h2"}) //设置Repository所在位置
public class H2Config {

    @Autowired
    @Qualifier("h2DataSource")
    private DataSource h2DataSource;

    @Bean(name = "entityManagerH2")
    public EntityManager entityManager(EntityManagerFactoryBuilder builder) {
        return entityManagerFactoryH2(builder).getObject().createEntityManager();
    }

    @Bean(name = "entityManagerFactoryH2")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryH2(EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(h2DataSource)
                .properties(getVendorProperties(h2DataSource))
                .packages("com.breakday.etag.h2") //设置实体类所在位置
                .persistenceUnit("h2PersistenceUnit")
                .build();
    }

    @Autowired
    private JpaProperties jpaProperties;

    private Map<String, String> getVendorProperties(DataSource dataSource) {
        return jpaProperties.getHibernateProperties(dataSource);
    }

    @Bean(name = "transactionManagerH2")
    PlatformTransactionManager transactionManagerH2(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(entityManagerFactoryH2(builder).getObject());
    }
}