package com.breakday.etag;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MyProperties {

    @Value("${SdkServer.ip}")
    public String ip;

    @Value("${SdkServer.sendProcess.sleep}")
    public long sendProcessSleep;

    @Value("${SdkServer.sendFailMaxNum}")
    public int sendFailMaxNum;

    @Value("${SdkServer.resultProcess.sleep}")
    public long resultProcessSleep;

    @Value("${SdkServer.template.normal}")
    public String templateNormal;

    @Value("${SdkServer.template.lowInventory}")
    public String templateLowInventory;

    @Value("${SdkServer.template.nearlyEffective}")
    public String templateNearlyEffective;

    @Value("${SdkServer.packageMaxNum}")
    public int packageMaxNum;

}