package com.breakday.etag;

import com.breakday.etag.h2.PoolService;
import com.breakday.etag.h2.StationService;
import com.breakday.etag.h2.TagService;
import com.breakday.etag.h2.TemplateService;
import com.halation.sdk.Result;
import com.halation.sdk.common.Constants;
import com.halation.sdk.delimiter.server.SdkServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class SdkInvoke {

    private static final Logger logger = LoggerFactory.getLogger(SdkInvoke.class);

    @Autowired
    private TagService tagService;

    @Autowired
    private TemplateService templateService;

    @Autowired
    private PoolService poolService;

    @Autowired
    private StationService stationService;

    @Autowired
    private Environment env;

    @Autowired
    private MyProperties myProperties;

    @Bean
    public SdkServer run() throws Exception {
        // 启动sdk服务
        SdkServer skdServer = SdkServer.getSdkServer();
        Result res = skdServer.startService(myProperties.ip, Constants.SDK_PORT);

        if (Result.ok.equals(res)) {

            logger.info("【ET】Server Start Success");

            // 开启刷新标签线程
            SetDataThread resultSetData = new SetDataThread();
            resultSetData.init(tagService, templateService, myProperties, poolService, stationService);
            resultSetData.start();

            // 开启接收线程
            CacheDataThread result = new CacheDataThread();
            result.init(tagService, myProperties);
            result.start();

        } else {
            logger.info("【ET】Server Start Failed");
            skdServer.stopServer();
        }

        return skdServer;
    }
}