package com.breakday.etag;

import com.breakday.etag.h2.*;
import com.halation.sdk.common.PackageCacheMap;
import com.halation.sdk.entity.TagEntity;
import com.halation.sdk.enums.ErrorCorrectionLevel;
import com.halation.sdk.enums.PatternType;
import com.halation.smartmk.business.entity.RefreshBusiness;
import com.halation.smartmk.business.entity.RefreshQueueData;
import net.sf.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class SetDataThread extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(SetDataThread.class);

    private TagService tagService;

    private TemplateService templateService;

    private PoolService poolService;

    private StationService stationService;

    private MyProperties myProperties;

    public void init(TagService tagService, TemplateService templateService, MyProperties myProperties, PoolService poolService, StationService stationService) {
        this.tagService = tagService;
        this.templateService = templateService;
        this.myProperties = myProperties;
        this.poolService = poolService;
        this.stationService = stationService;
    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(myProperties.sendProcessSleep);
                logger.info("【ET】发送看守线程(" + myProperties.sendProcessSleep + ")");
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            // 全屏刷新标签数据
            printCacheParallel(myProperties, tagService, templateService);
        }
    }

    /**
     * 做成刷新标签数据及刷新标签.
     *
     * @param myProperties
     * @param tagService
     * @param templateService
     */
    public void printCacheParallel(MyProperties myProperties, TagService tagService, TemplateService templateService) {

        // 待刷新的标签（取得方式各系统自己定义，可以是数据库/文件等方式）
        List<Tag> listTags = tagService.findAll();
        logger.info("【ET】取Tag数据：" + listTags.size());

        listTags = getTagsByTags(listTags, myProperties.sendFailMaxNum);
        logger.info("【ET】过滤Tag数据：" + listTags.size());

        if (listTags == null || listTags.size() == 0) {
            return;
        }

        List<Station> listStations = stationService.findAll();
        logger.info("【ET】取Station数据：" + listStations.size());

        if (listStations == null || listStations.size() == 0) {
            return;
        }

        List<Template> listTemplates = templateService.findAll();
        logger.info("【ET】取Template数据：" + listTemplates.size());

        if (listTemplates == null || listTemplates.size() == 0) {
            return;
        }

        Collections.sort(listTags, new Comparator<Tag>() {
            @Override
            public int compare(Tag arg0, Tag arg1) {
                return arg0.getTag().compareTo(arg1.getTag());
            }
        });

        Map<String, List<Tag>> stationListMap = new HashMap<String, List<Tag>>();
        List<Tag> apTags = new ArrayList<Tag>();
        apTags.add(listTags.get(0));
        stationListMap.put(listTags.get(0).getStation(), apTags);
        for (int i = 1; i < listTags.size(); i++) {
            if (stationListMap.get(listTags.get(i).getStation()) != null) {
                stationListMap.get(listTags.get(i).getStation()).add(listTags.get(i));
            } else {
                List<Tag> apNewTags = new ArrayList<Tag>();
                apNewTags.add(listTags.get(i));
                stationListMap.put(listTags.get(i).getStation(), apNewTags);
            }
        }

        // 每一包最大发送的标签数
        int tagNUm = myProperties.packageMaxNum;

        List<List<Tag>> offsetList = new ArrayList<List<Tag>>();
        for (String key : stationListMap.keySet()) {
            List<Tag> tagApList = new ArrayList<Tag>();
            tagApList = stationListMap.get(key);

            List<Tag> offsetTags = new ArrayList<Tag>();
            // 计算标签偏移量，如果偏移量超过FFFF，则分两次下发数据
            String firstTagId = tagApList.get(0).getTag();
            int hex1 = Integer.parseInt(firstTagId, 16);
            offsetTags.add(tagApList.get(0));
            for (int i = 1; i < tagApList.size(); i++) {
                int hex2 = Integer.parseInt(tagApList.get(i).getTag(), 16);
                // 标签ID偏移量超过FFFF
                if (hex2 - hex1 > 65535) {
                    offsetList.add(offsetTags);
                    offsetTags = new ArrayList<Tag>();
                    hex1 = Integer.parseInt(tagApList.get(i).getTag(), 16);
                }
                offsetTags.add(tagApList.get(i));
            }
            offsetList.add(offsetTags);
        }

        // 根据取得的要刷新标签数分包
        List<List<Tag>> sendTagList = new ArrayList<List<Tag>>();
        for (int i = 0; i < offsetList.size(); i++) {
            List<Tag> lst = offsetList.get(i);
            List<Tag> sendTags = new ArrayList<Tag>();

            for (int j = 0; j < lst.size(); j++) {
                sendTags.add(lst.get(j));

                // 标签数量等于200或者最后标签时，将标签追加到包数据里
                if ((j + 1) == tagNUm || (j + 1) == lst.size()) {
                    sendTagList.add(sendTags);
                    sendTags = new ArrayList<Tag>();
                }
            }
        }

        // 如果有多包数据，循环包数量，把各包数据传递给SDK
        for (int i = 1; i <= sendTagList.size(); i++) {

            // 刷新数据
            List<TagEntity> taglist = null;
            // 发送数据的基站编号(6位），根据需要自行设定
            String stationId = "000001";
            stationId = sendTagList.get(i - 1).get(0).getStation();

            // 设定标签数据
            // taglist = getCacheTaglist(sendTagList.get(i - 1), i);
            taglist = getCacheDataList(sendTagList.get(i - 1), i, listTemplates);

            // 刷新combo标签
            // taglist = getCacheCombolist(sendTagList.get(i-1), i);

            // 刷新数据集
            List<RefreshBusiness> stationDataList = new ArrayList<RefreshBusiness>();
            // 刷新数据
            RefreshBusiness data = new RefreshBusiness();
            // 监听基站编号
            List<String> listenStationIdList = new ArrayList<String>();//清缓存

            data.setListenStationIdList(listenStationIdList);
            data.setApNo(stationId);//基站编号
            data.setTemplateDataList(taglist);
            data.setPackageId(i);
            int power = 4;
            List<Station> stations = getPowerByStation(stationId, listStations);
            if (stations != null && stations.size() > 0) {
                power = stations.get(0).getPower();
            }
            data.setTransmitPower(power);
            stationDataList.add(data);

            RefreshQueueData refreshQueueData = new RefreshQueueData();
            refreshQueueData.setRefreshBusinessList(stationDataList);
            // 是否路由模式发送（true：路由模式，false：普通模式）
            refreshQueueData.setSendPackageDataUseRouteSign(true);

            PackageCacheMap.putInPackage(refreshQueueData, false);
        }
    }

    private List<Template> getTemplateByTag(String type, List<Template> listTemplates) {

        // 存放过滤结果的列表
        List<Template> result = null;

        // 使用lambda表达式过滤出结果并放到result列表里
        result = listTemplates.stream()
                .filter((Template b) -> type.contains(b.getType()))
                .collect(Collectors.toList());
        return result;
    }

    private List<Station> getPowerByStation(String stationId, List<Station> listStations) {

        // 存放过滤结果的列表
        List<Station> result = null;

        // 使用lambda表达式过滤出结果并放到result列表里
        result = listStations.stream()
                .filter((Station b) -> stationId.contains(b.getStation()))
                .collect(Collectors.toList());
        return result;
    }

    private List<Tag> getTagsByTags(List<Tag> Tags, int maxNum) {

        // 存放过滤结果的列表
        List<Tag> result = null;

        // 使用lambda表达式过滤出结果并放到result列表里
        result = Tags.stream()
                .filter((Tag b) -> {
                    return (b.getSendnum() < maxNum) && (b.getSendstatus() < 2);
                })
                .collect(Collectors.toList());
        return result;
    }

    private String getPoolFieldValue(Object obj, String name) {
        try {
            Class<?> class1 = Class.forName("com.breakday.etag.h2.Pool");
            Field field = class1.getDeclaredField(name);
            // System.out.println("******" + field.getName());
            field.setAccessible(true);
            // System.out.println("******" + field.get(obj));
            if (name.indexOf("avail_date") == 0) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                return sdf.format(field.get(obj));
            } else {
                return field.get(obj).toString();
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return null;
        } catch (SecurityException e) {
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Pool getOnlyPool(List<Pool> pools) {
        Pool pool = new Pool();
        pool = pools.get(0);
        for (int i = 1; i < pools.size(); i++) {
            pool.setStocknum(pool.getStocknum() + pools.get(i).getStocknum());
            logger.info("【ET】库存数据求和（" + pool.getStocknum() + "）");
        }
        return pool;
    }

    /**
     * 设定普通标签数据.
     *
     * @param tag           下发标签
     * @param listTemplates
     * @return 标签数据
     */
    private List<Map<String, String>> getCacheTaglist(Tag tag, List<Template> listTemplates) {

        List<Map<String, String>> dataMapList = new ArrayList<Map<String, String>>();

        List<Pool> pools = poolService.findByLocatorcode(tag.getLocatorcode());
        Pool pool = new Pool();
        String tType = "";
        if (pools == null || pools.size() == 0) {
            pool = null;
            tType = "";
            logger.info("【ET】根据Tag（" + tag.getStation() + " + " + tag.getTag() + "）的货位编码（" + tag.getLocatorcode() + "）在Pool里没有取到数据！");
        } else {
            pool = getOnlyPool(pools);
            tagService.updateTagByGoods(tag.getTag(), tag.getStation(), pool.getGoodsname());
            if (pool.getNeardate_flag() > 0) {
                tType = myProperties.templateNearlyEffective;
            } else if (pool.getLowstock_flag() > 0) {
                tType = myProperties.templateLowInventory;
            } else {
                tType = myProperties.templateNormal;
            }
        }

        List<Template> templates = getTemplateByTag(tType, listTemplates);

        if (templates == null || templates.size() == 0) {
            // TEXT项目
            Map<String, String> dataMap = new HashMap<String, String>();
            dataMap.put("data", "DEMO");
            dataMap.put("fieldType", "Text");
            dataMap.put("font", "f24px");
            dataMap.put("x", "10");
            dataMap.put("y", "10");
            dataMapList.add(dataMap);

            dataMap = new HashMap<String, String>();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = sdf.format(new Date());
            dataMap.put("data", date);
            dataMap.put("fieldType", "Text");
            dataMap.put("font", "f12px");
            dataMap.put("x", "80");
            dataMap.put("y", "80");
            dataMapList.add(dataMap);

            // 价格项目
            dataMap = new HashMap<String, String>();
            dataMap.put("data", "45.50");
            dataMap.put("fieldType", "Price");
            dataMap.put("font", "p28px");
            dataMap.put("x", "30");
            dataMap.put("y", "50");
            dataMapList.add(dataMap);

            // 二维码项目
            dataMap = new HashMap<String, String>();
            dataMap.put("data", "www.baidu.com");
            dataMap.put("fieldType", "QRCode");
            dataMap.put("font", "q45px");
            dataMap.put("errorCorrectionLevelContent", ErrorCorrectionLevel.M.toString());
            dataMap.put("x", "50");
            dataMap.put("y", "20");
            dataMapList.add(dataMap);
        } else {
            Map<String, String> dataMap = new HashMap<String, String>();
            Map<String, String> dataMapLast = new HashMap<String, String>();
            for (int i = 0; i < templates.size(); i++) {
                if (templates.get(i).getFieldtype().indexOf("QRCode") == 0) {
                    if (templates.get(i).getHisfield() == null || templates.get(i).getHisfield().isEmpty()) {
                        dataMapLast.put("data", templates.get(i).getData());
                    } else {
                        dataMapLast.put("data", pool == null ? "数据错误" : getPoolFieldValue(pool, templates.get(i).getHisfield()));
                    }
                    dataMapLast.put("fieldType", "QRCode");
                    dataMapLast.put("font", templates.get(i).getFont());
                    dataMapLast.put("errorCorrectionLevelContent", ErrorCorrectionLevel.M.toString());
                    dataMapLast.put("x", templates.get(i).getX());
                    dataMapLast.put("y", templates.get(i).getY());
                    dataMapLast.put("invertColor", templates.get(i).getInvertcolor());
                    dataMapLast.put("backColor", templates.get(i).getBackcolor());
                } else {
                    dataMap = new HashMap<String, String>();
                    if (templates.get(i).getHisfield() == null || templates.get(i).getHisfield().isEmpty()) {
                        dataMap.put("data", templates.get(i).getData());
                    } else {
                        dataMap.put("data", pool == null ? "数据错误" : getPoolFieldValue(pool, templates.get(i).getHisfield()));
                    }
                    if (templates.get(i).getFieldtype().indexOf("Price") == 0) {
                        dataMap.put("fieldType", "Price");
                    } else {
                        dataMap.put("fieldType", "Text");
                    }
                    dataMap.put("font", templates.get(i).getFont());
                    dataMap.put("x", templates.get(i).getX());
                    dataMap.put("y", templates.get(i).getY());
                    dataMap.put("invertColor", templates.get(i).getInvertcolor());
                    dataMap.put("backColor", templates.get(i).getBackcolor());
                }
                dataMapList.add(dataMap);
            }
            if (!dataMapLast.isEmpty() && dataMapLast != null) {
                dataMapList.add(dataMapLast);
            }
        }

        return dataMapList;
    }

    /**
     * 做成普通标签下发数据.
     *
     * @param sendTags
     * @param ii
     * @param listTemplates
     * @return 标签下发数据
     */
    private List<TagEntity> getCacheDataList(List<Tag> sendTags, int ii, List<Template> listTemplates) {

        List<TagEntity> lstTag = new ArrayList<TagEntity>();
        for (int i = 0; i < sendTags.size(); i++) {
            TagEntity tagEntity = new TagEntity();
            tagEntity.setTagId(sendTags.get(i).getTag()); // 标签ID
            tagEntity.setTagType(sendTags.get(i).getType());
            //tagEntity.setApNo("000001"); // 接收信号最强的基站编号
            tagEntity.setApNo(sendTags.get(i).getStation());
            tagEntity.setPattern(PatternType.UpdatedPattern); // 刷新命令
            String json = JSONArray.fromObject(getCacheTaglist(sendTags.get(i), listTemplates)).toString();
            tagEntity.setTemplateData(json); // 标签上要显示的内容
            logger.info("【ET】内容JSON:" + json);
            tagEntity.setUpdateStationId(false);
            lstTag.add(tagEntity);
        }
        return lstTag;
    }
}