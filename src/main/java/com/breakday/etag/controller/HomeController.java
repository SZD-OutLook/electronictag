package com.breakday.etag.controller;

import com.breakday.etag.h2.*;
import com.breakday.etag.mysql.Site;
import com.breakday.etag.mysql.SiteService;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;

import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.List;

@Api(value = "ET API 模块")
@RestController
public class HomeController {

    //@Autowired
    //private SiteService siteService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private TagService tagService;

    @Autowired
    private PoolService poolService;

    /**
     * /user/info
     *
     * @return
     */
    @RequestMapping(name = "/user/info", value = "/user/info", method = RequestMethod.GET)
    Object userInfo(String token) {
        String str = "{" +
                "  \"code\": 20000," +
                "  \"data\": {" +
                "    \"roles\": [" +
                "      \"admin\"" +
                "    ]," +
                "    \"role\": [" +
                "      \"admin\"" +
                "    ]," +
                "    \"name\": \"admin\"," +
                "    \"avatar\": \"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif\"" +
                "  }" +
                "}";

        return JSONObject.fromObject(str);
    }

    @RequestMapping(name = "/user/login", value = "/user/login", method = RequestMethod.POST)
    Object userLogin(@RequestBody User user) {
        String str = "{" +
                "  \"code\": 20000," +
                "  \"data\": {" +
                "    \"token\": \"admin\"" +
                "  }" +
                "}";

        return JSONObject.fromObject(str);
    }

    @RequestMapping(name = "/user/logout", value = "/user/logout", method = RequestMethod.POST)
    Object userLogout() {
        String str = "{" +
                "  \"code\": 20000," +
                "  \"data\": \"success\"" +
                "}";

        return JSONObject.fromObject(str);
    }

    /**
     * Site数据列表
     *
     * @return
     */
    //@ApiOperation(value = "Site数据列表", notes = "findAll()")
    //@RequestMapping(name = "/getAllBySite", value = "/getAllBySite", method = RequestMethod.GET)
    //List<Site> getAllBySite(){
    //    return siteService.findAll();
    //}

    /**
     * 添加一个站点
     *
     * @param name
     * @return
     */
    //@ApiOperation(value = "添加一个站点", notes = "传入name")
    //@RequestMapping(name = "/addSite", value = "/addSite", method = RequestMethod.POST)
    //@ApiImplicitParam(name = "name", value = "站点名", required = true, paramType = "body", dataType = "string") //注意：paramType需要指定为body
    //@ResponseBody
    //String addSite(@ApiParam(value = "站点名", required = true) @RequestBody String name){
    //    Site site = new Site();
    //    site.setAccount("aaa123");
    //    site.setMsg("哦哦");
    //    site.setName(name);
    //    site.setPass("123");
    //    site.setUrl("http://www.baidu.com");
    //    siteService.save(site);
    //    return "success";
    //}

    /**
     * 人员数据列表
     *
     * @return
     */
    @ApiOperation(value = "人员数据列表", notes = "employee findAll()")
    @RequestMapping(name = "/getAllByEmployee", value = "/getAllByEmployee", method = RequestMethod.GET)
    List<Employee> getAllByEmployee() {
        return employeeService.findAll();
    }

    /**
     * 保存一个Employee
     *
     * @param employee
     * @return
     */
    @ApiOperation(value = "保存一个Employee", notes = "ID:主键(自增长)，EMPCODE:可以重复(不能为空)，EMPNAME:不可为空。ID相同则修改记录。")
    @RequestMapping(name = "/saveEmployee", value = "/saveEmployee", method = RequestMethod.POST)
    @ApiImplicitParam(name = "employee", value = "employee", required = true, paramType = "body", dataType = "Employee")
    //注意：paramType需要指定为path,不然不能正常获取
    @ResponseBody
    String saveEmployee(@ApiParam(value = "employee", required = true) @RequestBody Employee employee) {
        employeeService.save(employee);
        return "success";
    }

    /**
     * 获取Tag List
     *
     * @return
     */
    @RequestMapping(name = "/getAllByTag", value = "/getAllByTag", method = RequestMethod.GET)
    List<Tag> getAllByTag() {
        return tagService.findAll();
    }

    /**
     * 手动发送
     *
     * @param tag
     * @param station
     * @return
     */
    @RequestMapping(name = "/updateTagBySend", value = "updateTagBySend", method = RequestMethod.POST)
    List<Tag> updateTagBySend(String tag, String station) {
        return tagService.updateTagBySend(tag, station, 0, 0, "") == 1 ? tagService.findByKey(tag, station) : null;
    }

    /**
     * 修改货位编码
     *
     * @param tag
     * @param station
     * @param locatorcode
     * @return
     */
    @RequestMapping(name = "/updateTagByLocatorCode", value = "updateTagByLocatorCode", method = RequestMethod.POST)
    List<Tag> updateTagByLocatorCode(String tag, String station, String locatorcode) {
        return tagService.updateTagByLocatorCode(tag, station, locatorcode) == 1 ? tagService.findByKey(tag, station) : null;
    }

    @RequestMapping(name = "/excelImportByTags", value = "excelImportByTags", method = RequestMethod.POST)
    List<Tag> excelImportByTags(@RequestBody List<Tag> tags) {
        List<Tag> result = new ArrayList<Tag>();
        for (int i = 0; i < tags.size(); i++) {
            if (tags.get(i).getTag() == null || tags.get(i).getTag().isEmpty()) {
                tags.get(i).setFailure("Tag不能为空");
                result.add(tags.get(i));
            } else if (tags.get(i).getStation() == null || tags.get(i).getStation().isEmpty()) {
                tags.get(i).setFailure("Station不能为空");
                result.add(tags.get(i));
            } else if (tags.get(i).getType() == null || tags.get(i).getType().isEmpty()) {
                tags.get(i).setFailure("Type不能为空");
                result.add(tags.get(i));
            } else if (tags.get(i).getLocatorcode() == null || tags.get(i).getLocatorcode().isEmpty()) {
                tags.get(i).setFailure("Locatorcode不能为空");
                result.add(tags.get(i));
            } else {
                List<Tag> tempList = tagService.findByKey(tags.get(i).getTag(), tags.get(i).getStation());
                Tag temp = new Tag();
                if (tempList != null && tempList.size() > 0) { // 说明已经存在
                    tags.get(i).setId(tempList.get(0).getId());
                    temp = tagService.save(tags.get(i));
                    temp.setFailure("update");
                    result.add(temp);
                } else {
                    tags.get(i).setId(0);
                    temp = tagService.save(tags.get(i));
                    temp.setFailure("insert");
                    result.add(temp);
                }
            }
        }

        return result;
    }

    /**
     * 保存一个Pool
     *
     * @param pool
     * @return
     */
    @ApiOperation(value = "保存一个Pool", notes = "ID:自增长，LOCATORCODE, PRODUCE_CODE:为联合主键。ID相同则修改记录。")
    @RequestMapping(name = "/savePool", value = "/savePool", method = RequestMethod.POST)
    @ApiImplicitParam(name = "pool", value = "pool", required = true, paramType = "body", dataType = "Pool")
    //注意：paramType需要指定为path,不然不能正常获取
    @ResponseBody
    String savePool(@ApiParam(value = "pool", required = true) @RequestBody Pool pool) {
        poolService.save(pool);
        return "success";
    }

    /**
     * 保存多个Pool
     *
     * @param pools
     * @return
     */
    @ApiOperation(value = "保存多个Pool", notes = "ID:自增长，LOCATORCODE, PRODUCE_CODE:为联合主键。ID相同则修改记录。")
    @RequestMapping(name = "/savePools", value = "/savePools", method = RequestMethod.POST)
    @ApiImplicitParam(name = "pools", value = "pools", required = true, paramType = "body", dataType = "List<Pool>")
    //注意：paramType需要指定为path,不然不能正常获取
    @ResponseBody
    List<PoolResult> savePools(@ApiParam(value = "pools", required = true) @RequestBody List<Pool> pools) {
        List<PoolResult> result = new ArrayList<PoolResult>();
        for (int i = 0; i < pools.size(); i++) {
            PoolResult poolResult = new PoolResult();
            poolResult.setId(pools.get(i).getId());
            poolResult.setLocatorcode(pools.get(i).getLocatorcode());
            poolResult.setProduce_code(pools.get(i).getProduce_code());
            if (pools.get(i).getLocatorcode() == null || pools.get(i).getLocatorcode().isEmpty()) {
                poolResult.setError("Locatorcode不能为空");
                result.add(poolResult);
            } else if (pools.get(i).getProduce_code() == null || pools.get(i).getProduce_code().isEmpty()) {
                poolResult.setError("Produce_code不能为空");
                result.add(poolResult);
            } else {
                List<Pool> tempList = poolService.findByKey(pools.get(i).getLocatorcode(), pools.get(i).getProduce_code());
                Pool temp = new Pool();
                List<Tag> tags = new ArrayList<Tag>();
                if (tempList != null && tempList.size() > 0) { // 说明已经存在
                    pools.get(i).setId(tempList.get(0).getId());
                    poolResult.setSuccess("update");
                } else {
                    pools.get(i).setId(0);
                    poolResult.setSuccess("insert");
                }
                temp = poolService.save(pools.get(i));
                poolResult.setId(temp.getId());
                result.add(poolResult);
                tags = tagService.findByLocatorCode(temp.getLocatorcode());
                if (tags != null && tags.size() > 0) { // 说明已经存在
                    tagService.updateTagBySend(tags.get(0).getTag(), tags.get(0).getStation(), 0, 0, "");
                }
            }
        }
        return result;
    }

    /**
     * getPoolByKey
     *
     * @param locatorcode
     * @param produce_code
     * @return
     */
    @RequestMapping(name = "/getPoolByKey", value = "getPoolByKey", method = RequestMethod.POST)
    List<Pool> getPoolByKey(String locatorcode, String produce_code) {
        return poolService.findByKey(locatorcode, produce_code);
    }

    /**
     * getAllByPool
     *
     * @return
     */
    @RequestMapping(name = "/getAllByPool", value = "/getAllByPool", method = RequestMethod.GET)
    List<Pool> getAllByPool() {
        return poolService.findAll();
    }
}

class User {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

class PoolResult {
    private int id;
    String produce_code;//批号
    String locatorcode;//货位编码
    String error;
    String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProduce_code() {
        return produce_code;
    }

    public void setProduce_code(String produce_code) {
        this.produce_code = produce_code;
    }

    public String getLocatorcode() {
        return locatorcode;
    }

    public void setLocatorcode(String locatorcode) {
        this.locatorcode = locatorcode;
    }
}