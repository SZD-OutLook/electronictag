package com.breakday.etag.h2;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeService extends CrudRepository<Employee, Integer> {

    public List<Employee> findAll();
    public Employee findOne(Integer id);
    public Employee save(Employee employee);
    public void delete(Integer id);

}
