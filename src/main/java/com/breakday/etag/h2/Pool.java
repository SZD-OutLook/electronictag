package com.breakday.etag.h2;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "POOL")
public class Pool {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    String storagecode;//药库编码、药局编码
    String storagename;//药库名称、药局名称
    String goods;//药品编码
    String goodsname;//药品名称
    String spec;//规格
    String producer;//厂家
    int stocknum;//库存数量
    String drugflagname;//药品类型
    Date avail_date;//近效期
    String produce_code;//批号
    int neardate_flag;//是否近效期
    int lowstock_flag;//是否低库存
    String locatorcode;//货位编码

    public String getStoragecode() {
        return storagecode;
    }

    public void setStoragecode(String storagecode) {
        this.storagecode = storagecode;
    }

    public String getStoragename() {
        return storagename;
    }

    public void setStoragename(String storagename) {
        this.storagename = storagename;
    }

    public String getGoods() {
        return goods;
    }

    public void setGoods(String goods) {
        this.goods = goods;
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public int getStocknum() {
        return stocknum;
    }

    public void setStocknum(int stocknum) {
        this.stocknum = stocknum;
    }

    public String getDrugflagname() {
        return drugflagname;
    }

    public void setDrugflagname(String drugflagname) {
        this.drugflagname = drugflagname;
    }

    public String getAvail_date() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(avail_date);
    }

    public void setAvail_date(Date avail_date) {
        this.avail_date = avail_date;
    }

    public String getProduce_code() {
        return produce_code;
    }

    public void setProduce_code(String produce_code) {
        this.produce_code = produce_code;
    }

    public int getNeardate_flag() {
        return neardate_flag;
    }

    public void setNeardate_flag(int neardate_flag) {
        this.neardate_flag = neardate_flag;
    }

    public int getLowstock_flag() {
        return lowstock_flag;
    }

    public void setLowstock_flag(int lowstock_flag) {
        this.lowstock_flag = lowstock_flag;
    }

    public String getLocatorcode() {
        return locatorcode;
    }

    public void setLocatorcode(String locatorcode) {
        this.locatorcode = locatorcode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
