package com.breakday.etag.h2;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PoolService extends CrudRepository<Pool, Integer> {

    public List<Pool> findAll();
    public Pool findOne(Integer id);
    public Pool save(Pool pool);
    public void delete(Integer id);

    @Query("select a from Pool a where a.locatorcode = ?1 order by a.avail_date")
    public List<Pool> findByLocatorcode(String locatorcode);

    @Query("select a from Pool a where a.locatorcode = ?1 and a.produce_code = ?2")
    public List<Pool> findByKey(String locatorcode, String produce_code);

}
