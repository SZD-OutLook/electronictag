package com.breakday.etag.h2;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StationService extends CrudRepository<Station, Integer> {

    public List<Station> findAll();
    public Station findOne(Integer id);
    public Station save(Station station);
    public void delete(Integer id);

}
