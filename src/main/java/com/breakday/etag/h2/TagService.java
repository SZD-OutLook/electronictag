package com.breakday.etag.h2;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
@Transactional
public interface TagService extends CrudRepository<Tag, Integer> {

    public List<Tag> findAll();
    public Tag findOne(Integer id);
    public Tag save(Tag tag);
    public void delete(Integer id);

    @Query("select a from Tag a where a.tag = ?1 and a.station = ?2")
    public List<Tag> findByKey(String tag, String station);

    @Query("select a from Tag a where a.locatorcode = ?1")
    public List<Tag> findByLocatorCode(String locatorcode);

    @Modifying
    @Query("update Tag a set a.sendnum = a.sendnum + ?3, a.successnum = a.successnum + ?7, a.sendstatus = ?4, a.failure = ?5, a.updatetime = ?6 where a.tag = ?1 and a.station = ?2")
    public int updateTag(String tag, String station, int sendnum, int sendstatus, String failure, Date updatetime, double successnum);

    @Modifying
    @Query("update Tag a set a.sendnum = ?3, a.sendstatus = ?4, a.failure = ?5 where a.tag = ?1 and a.station = ?2")
    public int updateTagBySend(String tag, String station, int sendnum, int sendstatus, String failure);

    @Modifying
    @Query("update Tag a set a.goodsname = ?3 where a.tag = ?1 and a.station = ?2")
    public int updateTagByGoods(String tag, String station, String goodsname);

    @Modifying
    @Query("update Tag a set a.locatorcode = ?3 where a.tag = ?1 and a.station = ?2")
    public int updateTagByLocatorCode(String tag, String station, String locatorcode);

}
