package com.breakday.etag.h2;

import javax.persistence.*;

@Entity
@Table(name = "TEMPLATE")
public class Template {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    String type;
    String fieldtype;
    String font;
    String x;
    String y;
    String data;
    String hisfield;
    String invertcolor;
    String backcolor;

    public String getInvertcolor() {
        return invertcolor;
    }

    public void setInvertcolor(String invertcolor) {
        this.invertcolor = invertcolor;
    }

    public String getBackcolor() {
        return backcolor;
    }

    public void setBackcolor(String backcolor) {
        this.backcolor = backcolor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFieldtype() {
        return fieldtype;
    }

    public void setFieldtype(String fieldtype) {
        this.fieldtype = fieldtype;
    }

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHisfield() {
        return hisfield;
    }

    public void setHisfield(String hisfield) {
        this.hisfield = hisfield;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
