package com.breakday.etag.h2;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TemplateService extends CrudRepository<Template, Integer> {

    public List<Template> findAll();
    public Template findOne(Integer id);
    public Template save(Tag Template);
    public void delete(Integer id);

}
