package com.breakday.etag.mysql;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SiteService extends CrudRepository<Site, Integer> {

    public List<Site> findAll();
    public Site findOne(Integer id);
    public Site save(Site site);
    public void delete(Integer id);

}
